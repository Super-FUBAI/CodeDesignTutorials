package cn.bugstack.design.test;

/**
* 抽象类
* @author Hush.
* @date 2022/9/13 10:15
*/
public class AbstractDemo1 {
    public static void main(String[] args) {

        //'Animal' 为 abstract；无法实例化
        //Animal animal1 = new Animal();


        // TODO Auto-generated method stub
        Animal animal = new Cat("23", 1) {
            public void cry() {
                System.out.println("猫叫:miao~miao~miao~");
            }
        };
        animal.cry();


        Mouth mouth1 = new Mouth() {
            @Override
            public String eat(String desc) {
                return null;
            }
        };
        Mouth mouth = (x)->{

            return "null"+x;
        };

        System.out.println(mouth.eat("顶顶顶顶顶"));
    }
}

// 这就是一个抽象类
abstract class Animal {
    String name;
    int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // 动物会叫
    public abstract void cry(); // 不确定动物怎么叫的。定义成抽象方法，来解决父类方法的不确定性。抽象方法在父类中不能实现，所以没有函数体。但在后续在继承时，要具体实现此方法。
}

// 抽象类可以被继承
// 当继承的父类是抽象类时，需要将抽象类中的所有抽象方法全部实现。
class Cat extends Animal{
    public Cat(String name, int age) {
        super(name, age);
    }

    // 实现父类的cry抽象方法
    public void cry() {
        System.out.println("猫叫:");

    }

}

//嘴巴接口
interface Mouth{
    String eat(String desc);
    //String eat2(String desc);
}

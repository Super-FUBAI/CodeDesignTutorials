package cn.bugstack.design.test;

import cn.bugstack.design.LoginSsoDecorator;
import cn.bugstack.design.SsoInterceptor;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ApiTest {

    @Test
    public void test_LoginSsoDecorator() {
        LoginSsoDecorator ssoDecorator = new LoginSsoDecorator(new SsoInterceptor());
        String request = "1successhuahua";
        boolean success = ssoDecorator.preHandle(request, "ewcdqwt40liuiu", "t");
        System.out.println("登录校验：" + request + (success ? " 放行" : " 拦截"));
    }

    @Test
    public void test_BufferedReader() throws FileNotFoundException {
        //从缓冲区之中读取内容，所有的输入的字节数据都将放在缓冲区之中
        BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Hush\\Pictures\\Saved Pictures\\成考\\确认表.jpg"));
    }

}

package cn.bugstack.design;


/**
 * 模拟的单点登录拦截服务
 *
 * @author Hush.
 * @date 2022/9/14 14:43
 */
public interface HandlerInterceptor {

    /**
     * 模拟单点登录拦截功能
     *
     * @param request:     请求
     * @param response:响应
     * @param handler:处理程序
     * @return boolean
     * @author Hush.
     * @since 2022/9/14 14:43
     */
    boolean preHandle(String request, String response, Object handler);

}

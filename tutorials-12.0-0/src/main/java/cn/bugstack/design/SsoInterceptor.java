package cn.bugstack.design;

/**
 * 模拟SSO的单点登录拦截服务
 *
 * @author Hush.
 * @date 2022/9/14 14:43
 */
public class SsoInterceptor implements HandlerInterceptor {


    public boolean preHandle(String request, String response, Object handler) {
        // 模拟获取cookie
        String ticket = request.substring(1, 8);
        // 模拟校验
        return ticket.equals("success");
    }

}

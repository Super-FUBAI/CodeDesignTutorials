package cn.bugstack.design;

/**
 * 博客：https://bugstack.cn - 沉淀、分享、成长，让自己和他人都能有所收获！
 * 公众号：bugstack虫洞栈
 * Create by 小傅哥(fustack) @2020
 * 优惠券折扣计算接口
 * <p>
 * 优惠券类型；
 * 1. 直减券
 * 2. 满减券
 * 3. 折扣券
 * 4. n元购
 */
public class CouponDiscountService {

    /**
     * 商品折扣问题
     *
     * @param type:        使用折扣类型
     * @param typeContent: 直减金额
     * @param skuPrice:    当前sku商品价格
     * @param typeExt:     满减金额
     * @return double 结算金额
     * @author Hush.
     * @since 2022/9/23 17:30
     */
    public double discountAmount(int type, double typeContent, double skuPrice, double typeExt) {

        // 1. 直减券
        if (1 == type) {
            return skuPrice - typeContent;
        }
        // 2. 满减券
        if (2 == type) {
            if (skuPrice < typeExt) return skuPrice;
            return skuPrice - typeContent;
        }
        // 3. 折扣券
        if (3 == type) {
            return skuPrice * typeContent;
        }
        // 4. n元购
        if (4 == type) {
            return typeContent;
        }
        return 0D;
    }

}

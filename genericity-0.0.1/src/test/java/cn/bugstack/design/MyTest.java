package cn.bugstack.design;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 深入理解Java泛型
 *
 * @author Hush.
 * @date 2022/9/19 14:28
 */
public class MyTest {
    private final Object[] arr = new Object[10];
    private int size;

    public void add(Object item) {
        arr[size++] = item;
    }

    public Object get(int index) {
        return arr[index];
    }

    @Override
    public String toString() {
        return "MyTest{" +
                "arr=" + Arrays.toString(arr) +
                ", size=" + size +
                '}';
    }

    @Test
    public void run() {
        MyTest myTest = new MyTest();
        myTest.add("241");
        myTest.add(123);
        myTest.add("sometime");
        myTest.add(new Object());
        System.out.println(myTest.get(0));
        System.out.println((int) myTest.get(2));//java.lang.ClassCastException

    }

    static class DataHolder<T> {
        T item;

        public void setData(T t) {
            this.item = t;
        }

        public T getData() {
            return this.item;
        }

        /**
         * 泛型方法
         *
         * @param e
         */
        public <E> void printe(E e) {
            System.out.println(e);
        }
    }

    @Test
    public void run2() {
        DataHolder<MyTest> myTestDataHolder = new DataHolder<>();
        MyTest myTest = new MyTest();
        myTest.add("241");
        myTestDataHolder.setData(myTest);
        myTestDataHolder.printe(myTest);
        myTestDataHolder.printe(1.00);

    }

    /**
     * Java泛型接口
     *
     * @author Hush.
     * @date 2022/9/19 14:58
     */
    interface Generator<T> {
        T next(T t);
    }



    @Test
    public void run3() {
        //Generator<String> generator = new Generator<String>() {
        //    @Override
        //    public String next(String s) {
        //        return "next"+s;
        //    }
        //};

        //Generator<String> generator = s -> "next"+s;

        System.out.println(((Generator<String>) s -> "next" + s).next("run3"));
    }

    /**
     * 泛型擦除
     * @author Hush.
     * @since 2022/9/19 15:42
     */
    @Test
    public void run4() {

        Class<?> class1=new ArrayList<String>().getClass();
        Class<?> class2=new ArrayList<Integer>().getClass();
        System.out.println(class1);		//class java.util.ArrayList
        System.out.println(class2);		//class java.util.ArrayList
        System.out.println(class1.equals(class2));	//true

    }

    class Fruit {}
    class Apple extends Fruit {}
    class RedApple extends Apple {}
    class Plate<T>{
        T item;
        public Plate(T t){
            item=t;
        }

        public void set(T t) {
            item=t;
        }
        public Plate<? extends Fruit> setExtends(Plate<? extends Fruit> p2) {
            return p2;
        }

        public T get() {
            return item;
        }
    }

    @Test
    public void run5() {
        //Plate<Fruit> p=new Plate<Apple>(new Apple());

        Plate<Fruit> p= new Plate<>(new Apple());
        p.set(new RedApple());

        Apple apple = new Apple();
        Plate<? extends Fruit> p2= new Plate<>(apple);
        Plate<? super RedApple> p3= new Plate<>(new RedApple());
        //这里怎么set一个对象
        p3.set(null);
    }


}

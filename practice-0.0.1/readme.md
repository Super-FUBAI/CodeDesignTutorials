
# 23种设计模式 

开始学习时间:2020年7月23日20:19:14
<table border="1">
<caption>
GoF 的 23 种设计模式的分类表</caption>
<tbody>
<tr>
<th>
范围\目的</th>
<th>
创建型模式</th>
<th>
结构型模式</th>
<th>
行为型模式</th>
</tr>
<tr>
<td>
类模式</td>
<td>
工厂方法</td>
<td>
(类）适配器</td>
<td>
模板方法、解释器</td>
</tr>
<tr>
<td colspan="1" rowspan="9">
对象模式</td>
<td colspan="1" rowspan="9">
单例<br>
原型<br>
抽象工厂<br>
建造者</td>
<td colspan="1" rowspan="7">
代理<br>
(对象）适配器<br>
桥接<br>
装饰<br>
外观<br>
享元<br>
组合</td>
<td colspan="1" rowspan="6">
策略<br>
命令<br>
职责链<br>
状态<br>
观察者<br>
中介者<br>
迭代器<br>
访问者<br>
备忘录</td>
</tr>
</tbody>
</table>
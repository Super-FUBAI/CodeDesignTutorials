package com.fubai.OOPDesign;

/**
 * @ClassTitle: LSPtest
 * @ProjectName Gof_23
 * @Description: 里氏替换原则
 * @Author Sangsang
 * @Date 2020/7/24 0024
 * @Time 10:19
 */
public class LSPtest {
	public static void main(String[] args)
	{
		Bird bird1=new Swallow();
		Animal bird2=new BrownKiwi();
		bird1.setSpeed(120);
		bird2.setRunSpeed(120);
		System.out.println("如果飞行300公里：");
		try
		{
			System.out.println("燕子将飞行"+bird1.getFlyTime(300)+"小时.");
			System.out.println("几维鸟将跑步进场"+bird2.getRunTime(300)+"小时。");
		}
		catch(Exception err)
		{
			System.out.println("发生错误了!");
		}
		/**
		 * 程序的运行结果如下:
		 *      如果飞行300公里：
		 *      燕子将飞行2.5小时.
		 *      几维鸟将飞行Infinity小时。
		*/
	}
}
//鸟类
class Bird
{
	double flySpeed;
	public void setSpeed(double speed)
	{
		flySpeed=speed;
	}
	public double getFlyTime(double distance)
	{
		return(distance/flySpeed);
	}
}
//动物类
class Animal
{
	double runSpeed;


	public void setRunSpeed(double runSpeed) {
		this.runSpeed = runSpeed;
	}

	public double getRunTime(double distance)
	{
		return(distance/runSpeed);
	}
}
//燕子类
class Swallow extends Bird{}
//几维鸟类
//class BrownKiwi extends Bird
class BrownKiwi extends Animal
{

}


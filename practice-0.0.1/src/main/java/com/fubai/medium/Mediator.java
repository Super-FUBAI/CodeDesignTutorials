package com.fubai.medium;

/**
* 中介者模式
* @author Hush.
* @date 2022/9/22 14:25
*/
public interface Mediator {
    // 各营在这里注册，接收各营发来的战况电报,建立通讯
    void receive(String name, Colleague colleague);

    // 下达命令
    void order(String name);
}

package com.fubai.medium;

/**
 * 抽象同事类角色 这里看成抽象的
 *
 * @author Hush.
 * @date 2022/9/22 14:26
 */
public interface Colleague {
    /**
     * 战斗方法
     * @author Hush.
     * @since 2022/9/22 14:28
     */
    void battle(String s);

    /**
     * 团队作业方法
     * @author Hush.
     * @since 2022/9/22 14:28
     */
    void teamWork();
}

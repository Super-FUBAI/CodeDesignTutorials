package com.fubai.medium.impl;

import com.fubai.medium.Colleague;
import com.fubai.medium.Mediator;

import java.util.HashMap;
import java.util.Map;

/**
* 具体中介者角色 李队长
* @author Hush.
* @date 2022/9/22 14:55
*/
public class CaptainLi implements Mediator {
    private final Map<String,Colleague> map = new HashMap<>();

    @Override
    public void receive(String name, Colleague colleague) {
        map.put(name, colleague);
    }

    @Override
    public void order(String name) {
        map.get(name).battle(name+"gogogo");
    }
}

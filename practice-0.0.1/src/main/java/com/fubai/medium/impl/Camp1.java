package com.fubai.medium.impl;

import com.fubai.medium.Colleague;
import com.fubai.medium.Mediator;

/**
* 战斗一营
* @author Hush.
* @date 2022/9/22 14:33
*/
public class Camp1 implements Colleague {

    private Mediator mediator;


    public Camp1(Mediator mediator) {
        this.mediator = mediator;
        mediator.receive("Camp1", this);
    }

    /**
     * 战斗方法
     *
     * @author Hush.
     * @since 2022/9/22 14:28
     */
    @Override
    public void battle(String s) {
        System.out.println(this+"battle!!!");
        System.out.println(s);
    }

    /**
     * 团队作业方法
     *
     * @author Hush.
     * @since 2022/9/22 14:28
     */
    @Override
    public void teamWork() {
        mediator.order("Camp2");
    }
}

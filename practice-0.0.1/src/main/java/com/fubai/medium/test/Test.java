package com.fubai.medium.test;

import com.fubai.medium.Colleague;
import com.fubai.medium.Mediator;
import com.fubai.medium.impl.Camp1;
import com.fubai.medium.impl.Camp2;
import com.fubai.medium.impl.CaptainLi;

/**
 * 中介中者测试类
 * 李队长调用一营和二营处理平安县战役
 *
 * @author Hush.
 * @date 2022/9/22 14:58
 */
public class Test {

    public static void main(String[] args) {
        Mediator lic = new CaptainLi();
        Colleague c1 = new Camp1(lic);
        Colleague c2 = new Camp2(lic);

        c1.battle("c1 gogogogogo");
        c1.teamWork();

        //c2.battle("");
        //c2.teamWork();
    }
}

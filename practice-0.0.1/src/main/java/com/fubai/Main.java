package com.fubai;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here
        addWaterMark("C:\\Users\\Administrator\\Desktop\\1.jpg","C:\\Users\\Administrator\\Desktop\\temp\\1.jpg");
    }

    /**
     *
     *
     * @param srcImgPath 源图片路径
     * @param tarImgPath 保存的图片路径
     */
    public static void addWaterMark(String srcImgPath, String tarImgPath) {
        try {
            // 读取原图片信息
            File srcImgFile = new File(srcImgPath);//得到文件

            Image srcImg = ImageIO.read(srcImgFile);//文件转化为图片
            int srcImgWidth = srcImg.getWidth(null);//获取图片的宽
            int srcImgHeight = srcImg.getHeight(null);//获取图片的高
            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufImg.createGraphics();
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            Color markContentColor = new Color(250, 250, 250);//水印图片色彩以及透明度/255, 255, 0
            Font font = /*new Font("宋体", Font.PLAIN, 50);//水印字体*/
            new Font("微软雅黑", Font.BOLD, 16);// 添加字体的属性设置
            g.setColor(markContentColor); //根据图片的背景设置水印颜色
            g.setFont(font);              //设置字体
            g.drawString("潘子与嘎子.",10,16);//添加的内容，x，y
            g.dispose();
            // 输出图片
            FileOutputStream outImgStream = new FileOutputStream(tarImgPath);
            ImageIO.write(bufImg, "jpg", outImgStream);
            outImgStream.flush();
            outImgStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.fubai.abstractFactory;

/**
 * 键盘
 */
public interface IKeyboard {
	/**
	 * 产品名称
	 */
	void name();
	/**
	 * 厂家
	 */
	void vender();

	/**
	 * 价位
	 */
	void price();

	/**
	 * 敲击
	 */
	void rap();
}

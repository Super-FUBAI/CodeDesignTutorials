package com.fubai.abstractFactory;

public interface IProductFactory {
	//键盘
	IKeyboard keyboard();

	//鼠标
	IMouse mouse();

	//包
	IPackage bag();
}

package com.fubai.abstractFactory;

/**
 * 鼠标
 */
public interface IMouse {
	/**
	 * 产品名称
	 */
	void name();

	/**
	 * 厂家
	 */
	void vender();
	/**
	 * 价位
	 */
	void price();
	/**
	 * 点击
	 */
	void click();
}

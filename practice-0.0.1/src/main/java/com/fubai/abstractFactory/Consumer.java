package com.fubai.abstractFactory;

import com.fubai.factory.Car;
import com.fubai.factory.DongfengPeugeotFactory;

/**
 * @ClassTitle: Consumer
 * @ProjectName Gof_23
 * @Description: 消费者买键鼠
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 22:08
 */
public class Consumer {
	public static void main(String[] args) {
		IKeyboard keyboard = new AppleFactory().keyboard();
		keyboard.name();
		keyboard.vender();
		keyboard.price();
		keyboard.rap();

		IMouse mouse = new AppleFactory().mouse();
		mouse.name();
		mouse.vender();
		mouse.price();
		mouse.click();
	}

}

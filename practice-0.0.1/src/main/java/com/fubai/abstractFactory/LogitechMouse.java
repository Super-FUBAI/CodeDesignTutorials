package com.fubai.abstractFactory;

/**
 * @ClassTitle: LogitechMouse
 * @ProjectName Gof_23
 * @Description: 罗技鼠标
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 22:35
 */
public class LogitechMouse implements IMouse {
	@Override
	public void name() {
		System.out.println("Logitech鼠标");
	}

	@Override
	public void vender() {
		System.out.println("Logitech-vender");
	}

	@Override
	public void price() {
		System.out.println("Logitech-350RMB");
	}

	@Override
	public void click() {
		System.out.println("da~");
	}
}

package com.fubai.abstractFactory;

/**
 * 电脑包
 */
public interface IPackage {
	/**
	 * 产品名称
	 */
	void name();
	/**
	 * 厂家
	 */
	void vender();

	/**
	 * 价位
	 */
	void price();


}

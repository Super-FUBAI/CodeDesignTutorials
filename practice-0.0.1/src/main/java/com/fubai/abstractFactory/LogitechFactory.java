package com.fubai.abstractFactory;

public class LogitechFactory implements IProductFactory {
	@Override
	public IKeyboard keyboard() {
		return new LogitechKeyboard();
	}

	@Override
	public IMouse mouse() {
		return new LogitechMouse();
	}

	@Override
	public IPackage bag() {
		return null;
	}
}
//
//public class LogitechFactory implements IKeyboard,IMouse  {
//
//}

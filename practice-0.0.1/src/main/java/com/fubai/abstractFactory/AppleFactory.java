package com.fubai.abstractFactory;

/**
 * @ClassTitle: AppleFactory
 * @ProjectName Gof_23
 * @Description:
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 22:53
 */
public class AppleFactory implements IProductFactory {
	@Override
	public IKeyboard keyboard() {
		return new AppleKeyboard();
	}

	@Override
	public IMouse mouse() {
		return new AppleMouse();
	}

	@Override
	public IPackage bag() {
		return null;
	}
}

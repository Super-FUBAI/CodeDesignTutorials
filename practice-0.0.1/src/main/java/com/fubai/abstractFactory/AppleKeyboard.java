package com.fubai.abstractFactory;

/**
 * @ClassTitle: AppleMouse
 * @ProjectName Gof_23
 * @Description: Apple键盘
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 22:35
 */
public class AppleKeyboard implements IKeyboard {
	@Override
	public void name() {
		System.out.println("Apple键盘");

	}

	@Override
	public void vender() {
		System.out.println("Apple-vender");

	}

	@Override
	public void price() {
		System.out.println("Apple-400RMB");

	}

	@Override
	public void rap() {
		System.out.println("~~");

	}
/*	@Override
	public void name() {
		System.out.println("Apple鼠标");
	}

	@Override
	public void vender() {
		System.out.println("Apple-vender");
	}

	@Override
	public void price() {
		System.out.println("Apple-350RMB");
	}

	@Override
	public void click() {
		System.out.println("da~");
	}*/
}


Facade Pattern: Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

### 外观模式
#### 介绍: 为子系统中的一组接口提供一个统一的入口。外观模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。
    外观模式中，一个子系统的外部与其内部的通信通过一个统一的外观类进行，
    外观类将客户类与子系统的内部复杂性分隔开，
    使得客户类只需要与外观角色打交道，而不需要与子系统内部的很多对象打交道。


### 实例说明 https://blog.csdn.net/lovelion/article/details/8259705
    某软件公司欲开发一个可应用于多个软件的文件加密模块，该模块可以对文件中的数据进行加密并将加密之后的数据存储在一个新文件中，
    具体的流程包括三个部分,分别是读取源文件、加密、保存加密之后的文件，其中，读取文件和保存文件使用流来实现，加密操作通过求模运算实现。
    这三个操作相对独立，为了实现代码的独立重用，让设计更符合单一职责原则，这三个操作的业务代码封装在三个不同的类中。
    现使用外观模式设计该文件加密模块。

        



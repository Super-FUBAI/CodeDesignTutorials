package com.fubai.facadepattern;

/**
 * 外观模式 </p>
 * 理解:客户端直接通过外观类(服务员)接触各个子系统(不同菜系的厨师),实现各系统的功能(川菜,徽菜,鲁菜)
 *
 * @author Hush.
 * @since 2022/03/25 15:32
 */

class SubSystemA {
    public void MethodA() {
        //业务实现代码
        System.out.println("MethodA=MethodA");
    }
}

class SubSystemB {
    public void MethodB() {
        //业务实现代码
        System.out.println("MethodB=MethodB");

    }
}

class SubSystemC {
    public void MethodC() {
        //业务实现代码
        System.out.println("MethodC=MethodC");

    }
}

/**
 * 外观类
 * 在引入外观类之后，与子系统业务类之间的交互统一由外观类来完成
 */
class Facade {
    private SubSystemA subSystemA = new SubSystemA();
    private SubSystemB subSystemB = new SubSystemB();
    private SubSystemC subSystemC = new SubSystemC();

    public void method() {
        subSystemA.MethodA();
        subSystemB.MethodB();
        subSystemC.MethodC();
    }
}

/**
 * 客户端
 */
class Program {
    public static void main(String[] args) {
        Facade facade = new Facade();
        facade.method();
    }
}



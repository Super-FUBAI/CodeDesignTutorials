package com.fubai.facadepattern.demo.sub;

import java.io.*;

/**
 * 文件读取系统 </p>
 *
 * @author Hush.
 * @since 2022/03/25 16:10
 */
public class FileReadSub {

    public String read(String src){
        File f=new File(src);
        try {
            FileInputStream inputStream = new FileInputStream(f);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static void main(String[] args) {
        new FileReadSub().read("C:\\Users\\Hush\\Documents\\workspace\\ideaWorkspace\\demo\\Gof_23\\src\\main\\java\\com\\fubai\\facadepattern\\readme.md");
    }
}

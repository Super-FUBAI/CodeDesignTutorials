package com.fubai.strategy_mode;

/**
 * 具体算法类 继承 AbstractStrategy</p>
 *
 * @author Hush.
 * @since 2022/03/24 17:16
 */
public class ConcreteStrategyA extends AbstractStrategy {
    /**
     * 重写计算方法
     *
     * @author Hush.
     * @since 2022/3/24 17:17
     */
    @Override
    public void algorithm() {
        System.out.println(this.getClass().toString()+"ConcreteStrategyA的计算方法");
    }
}

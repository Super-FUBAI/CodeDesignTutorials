package com.fubai.strategy_mode;

/**
 * 对于Context类而言，在它与抽象策略类之间建立一个关联关系 </p>
 *
 * @author Hush.
 * @since 2022/03/24 17:22
 */
public class Context {

    /**
     * 维持一个对抽象策略类的引用
     */
    private AbstractStrategy strategy;

    public void setStrategy(AbstractStrategy strategy) {
        this.strategy= strategy;
    }

    /**
     * 调用策略类中的算法
     */
    public void algorithm() {
        strategy.algorithm();
    }


    /**
     * 这里做一个简单的测试
     */
    public static void main(String[] args) {
        Context context = new Context();
        /*
          这里你想实现那种方法你就new那个实现
          在客户端代码中只需注入一个具体策略对象，可以将具体策略类类名存储在配置文件中，通过反射来动态创建具体策略对象，
          从而使得用户可以灵活地更换具体策略类，增加新的具体策略类也很方便。策略模式提供了一种可插入式(Pluggable)算法的实现方案。
         */
        context.setStrategy(new ConcreteStrategyA());
        context.algorithm();
    }
}

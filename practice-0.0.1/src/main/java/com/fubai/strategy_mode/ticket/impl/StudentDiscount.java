package com.fubai.strategy_mode.ticket.impl;

import com.fubai.strategy_mode.ticket.Discount;

/**
 * 学生价 </p>
 *
 * @author Hush.
 * @since 2022/03/24 18:13
 */
public class StudentDiscount extends Discount {
    /**
     * 计算学生票价
     *
     * @return 计算结果
     * @author Hush.
     * @since 2022/3/24 18:21
     */
    @Override
    public double calculate(double price) {
        System.out.println("学生票：");
        return price * 0.8;
    }
}

package com.fubai.strategy_mode.ticket.impl;

import com.fubai.strategy_mode.ticket.Discount;

/**
 * 未成年价格 </p>
 *
 * @author Hush.
 * @since 2022/03/24 18:13
 */
public class ChildrenDiscount extends Discount {
    /**
     * 计算未成年价格票价
     *
     * @return 计算结果
     * @author Hush.
     * @since 2022/3/24 18:21
     */
    @Override
    public double calculate(double price) {
        System.out.println("儿童票：");
        return price - 10;

    }
}

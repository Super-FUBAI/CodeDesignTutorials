package com.fubai.strategy_mode.ticket.impl;

import com.fubai.strategy_mode.ticket.Discount;

/**
 * vip价格 </p>
 *
 * @author Hush.
 * @since 2022/03/24 18:14
 */
public class VIPDiscount extends Discount {
    /**
     * 计算vip价格
     *
     * @return 计算结果
     * @author Hush.
     * @since 2022/3/24 18:21
     */
    @Override
    public double calculate(double price) {
        System.out.println("VIP票：");
        System.out.println("增加积分！");
        return price * 0.5;

    }
}

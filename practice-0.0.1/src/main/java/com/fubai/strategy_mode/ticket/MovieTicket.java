package com.fubai.strategy_mode.ticket;

/**
 * 环境类角色 </p>
 *
 * @author Hush.
 * @since 2022/03/24 18:12
 */
public class MovieTicket {
    /**
     * 票价
     */
    private Double price;

    /**
     * 折扣
     */
    private Discount discount;

    public Double getPrice() {
        return discount.calculate(this.price);
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}

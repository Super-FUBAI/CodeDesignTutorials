package com.fubai.strategy_mode.ticket;

import com.fubai.strategy_mode.ticket.impl.ChildrenDiscount;
import com.fubai.strategy_mode.ticket.impl.VIPDiscount;

/**
 * 票价计算测试类 </p>
 *
 * @author Hush.
 * @since 2022/03/24 19:15
 */
public class Test {

    public static void main(String[] args) {


        /*
         * 原价
         */
        double originalPrice = 60.0;
        /*
         * 折扣价
         */
        double currentPrice;

        MovieTicket mt = new MovieTicket();
        mt.setPrice(originalPrice);
        /*
         * 注入折扣对象
         * 这里可以根据参数条件实现不同的计算模式
         */
        mt.setDiscount(new VIPDiscount());
        currentPrice = mt.getPrice();
        System.out.println("折后价为：" + currentPrice);

    }


}

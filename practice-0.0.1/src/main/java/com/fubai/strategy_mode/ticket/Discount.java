package com.fubai.strategy_mode.ticket;

/**
 * 抽象策略角色 </p>
 * 这里也可以通过接口来定义
 * @author Hush.
 * @since 2022/03/24 18:13
 */
public abstract class Discount {
   /**
    * 计算票价的抽象方法
    * @return 计算结果
    * @author Hush.
    * @since 2022/3/24 18:21
    */
    public abstract double calculate(double price);
}

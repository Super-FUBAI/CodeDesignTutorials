package com.fubai.strategy_mode;

/**
 * 抽象策略 抽象计算方法 </p>
 *
 * @author Hush.
 * @since 2022/03/24 17:16
 */
public abstract class AbstractStrategy {
    /**
     * 声明抽象算法
     * @author Hush.
     * @since 2022/3/24 17:17
     */
    public abstract void algorithm();

}

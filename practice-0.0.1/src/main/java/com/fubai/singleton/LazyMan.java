package com.fubai.singleton;

import java.lang.reflect.Constructor;

/**
 * @ClassTitle: LazyMan
 * @ProjectName Gof_23
 * @Description: 狂神的懒加载举例
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 17:24
 */
public class LazyMan {
	private LazyMan() {
		System.out.println(Thread.currentThread().getName() + "启动");
	}

	private volatile static LazyMan lazyMan;

	//获得实例
	public static LazyMan getInstance() {
		//加锁
		if (lazyMan == null) {
			/**
			 * 至于为什么在前面加一个if判断,而不是直接加锁 原因我给总结了一下
			 * 1,第一个if可以提高性能的，锁释放都会消耗很多资源
			 * 2,加上第一层，是为了提高性能，不能因为每次都在锁外面等候
			 */
			synchronized (LazyMan.class) {
				if (lazyMan == null) {
					lazyMan = new LazyMan();//非原子性操作
					/**
					 *产生下列操作
					 * 1,分配内存空间
					 * 2,执行构造方法,初始化对象
					 * 3,对象指定内存空间
					 * 这个可能会导致指令重排
					 * 解决方法 : volatile
					 */
				}
			}
		}

		return lazyMan;
	}

	public static void main(String[] args) throws Exception {
//		for (int i = 0; i < 10; i++) {
//			new Thread(() -> {
//				System.out.println(LazyMan.getInstance());
//			}).start();
//		}
		Constructor<LazyMan> declaredConstructor = LazyMan.class.getDeclaredConstructor();
		declaredConstructor.setAccessible(true);
		LazyMan lazyMan = declaredConstructor.newInstance();
	}

}

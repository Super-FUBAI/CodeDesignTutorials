package com.fubai.singleton;

/**
 * @ClassTitle: HungrySingleton
 * @ProjectName Gof_23
 * @Description: 饿汉式单例 主要作用是在项目中当作静态变量来使用
 * @Author Sangsang
 * @Date 2020/7/24 0024
 * @Time 15:52
 */
public class HungrySingleton {
	/**
	 * 加载就创建完成了
	 * 缺点：可能会浪费空间
	 */
	private static final HungrySingleton Hungry = new HungrySingleton();

	/**
	 * 构造器私有 本类中实例化
	 */
	private HungrySingleton() {
	}

	public static HungrySingleton getInstance() {
		return Hungry;
	}
}

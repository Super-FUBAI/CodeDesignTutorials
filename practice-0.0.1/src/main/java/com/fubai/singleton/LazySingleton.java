package com.fubai.singleton;

/**
 * @ClassTitle: LazySingleton
 * @ProjectName Gof_23
 * @Description: 懒汉式单例
 * @Author Sangsang
 * @Date 2020/7/24 0024
 * @Time 15:49
 */
public class LazySingleton {

	/*private static volatile LazySingleton instance=null;    //保证 instance 在所有线程中同步
	private LazySingleton(){}    //private 避免类在外部被实例化
	public static synchronized LazySingleton getInstance()
	{
		//getInstance 方法前加同步
		if(instance==null)
		{
			instance=new LazySingleton();
		}
		return instance;
	}*/


	public static void main(String[] args)
	{
		President zt1=President.getInstance();
		zt1.getName();    //输出总统的名字
		President zt2=President.getInstance();
		zt2.getName();    //输出总统的名字
		if(zt1==zt2)
		{
			System.out.println("他们是同一人！");
		}
		else
		{
			System.out.println("他们不是同一人！");
		}
	}
}
class President
{
	private static volatile President instance=null;    //保证instance在所有线程中同步
	//private避免类在外部被实例化
	private President()
	{
		System.out.println("产生一个总统！");
	}
	public static synchronized President getInstance()
	{
		//在getInstance方法上加同步
		if(instance==null)
		{
			instance=new President();
		}
		else
		{
			System.out.println("已经有一个总统，不能产生新总统！");
		}
		return instance;
	}
	public void getName()
	{
		System.out.println("我是美国总统：特朗普。");
	}
}
package com.fubai.singleton.enums;

/**
 * @ClassTitle: DBConnection
 * @ProjectName Gof_23
 * @Description: 单例枚举
 * @Author Hush. ultraman_zero@aliyun.com
 * @Date 2021/4/29
 * @Time 9:58
 */
public class DBConnection {
}



class Test {
	public static void main(String[] args) {
		DBConnection conn1 = DataSourceEnum.DATASOURCE.getConnection();
		DBConnection conn2 = DataSourceEnum.DATASOURCE.getConnection();
		System.out.println(conn1 == conn2);
	}
}
package com.fubai.factory;

/**
 * @ClassTitle: CarFactory
 * @ProjectName Gof_23
 * @Description: 工厂方法模式
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 21:34
 */
public interface CarFactory {
	public Car oneCar();
}

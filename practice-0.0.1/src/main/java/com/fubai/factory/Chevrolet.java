package com.fubai.factory;

/**
 * @ClassTitle: Chevrolet
 * @ProjectName Gof_23
 * @Description: 雪佛兰
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 21:56
 */
public class Chevrolet implements Car {
	@Override
	public String getCarName() {
		return "雪佛兰";
	}
//	public Chevrolet() {
//		System.out.println("雪佛兰!");
//	}
}

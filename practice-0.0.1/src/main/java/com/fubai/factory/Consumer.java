package com.fubai.factory;

/**
 * @ClassTitle: Consumer
 * @ProjectName Gof_23
 * @Description: 消费者买车
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 22:08
 */
public class Consumer {
    public static void main(String[] args) {
        //雪佛兰
        //Car car1 = new  ChevroletFactory().oneCar();
        //东风标致
        Car car1 = new DongfengPeugeotFactory().oneCar();
        System.out.println(car1.getCarName());
    }

}

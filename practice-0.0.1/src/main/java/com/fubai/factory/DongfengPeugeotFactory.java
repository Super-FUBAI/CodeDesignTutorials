package com.fubai.factory;

/**
 * @ClassTitle: ChevroletFactory
 * @ProjectName Gof_23
 * @Description: 雪佛兰工厂
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 22:03
 */
public class DongfengPeugeotFactory implements CarFactory {
	@Override
	public Car oneCar() {
		return new DongfengPeugeot();
	}
}

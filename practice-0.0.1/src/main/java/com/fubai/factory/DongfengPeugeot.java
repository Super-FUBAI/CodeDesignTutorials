package com.fubai.factory;

/**
 * @ClassTitle: Chevrolet
 * @ProjectName Gof_23
 * @Description: 雪佛兰
 * @Author Sangsang
 * @Date 2020/7/26
 * @Time 21:56
 */
public class DongfengPeugeot  implements Car {
	@Override
	public String getCarName() {
		return "东风标致";
	}
//	public DongfengPeugeot() {
//		System.out.println("东风标致!");
//	}
}

package com.fubai;

/**
 * @ClassTitle: BubbleSort
 * @ProjectName Gof_23
 * @Description: 冒泡排序
 * @Author Sangsang
 * @Date 2020/9/7
 * @Time 9:49
 */
//线程
public class BubbleSort {
	public static void main(String[] args) {
		// write your code here
		int[] array = {1, 2, 3, 10, 5, 6, 0, 9};
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length - 1- i ; j++) {
				if (array[j]>array[j+1]){
					int temp = array[j+1];
					array[j+1] = array[j];
					array[j] = temp;
				}
			}
		}
		for (int i : array) {
			System.out.println(i);
		}
		/*int temp = 0;
// 外层循环，它决定一共走几趟 //-1为了防止溢出
		for (int i = 0; i < array.length - 1; i++) {
			int flag = 0; //通过符号位可以减少无谓的比较，如果已经有序了，就退出循环
//内层循环，它决定每趟走一次
			for (int j = 0; j < array.length - i - 1; j++) {
//如果后一个大于前一个,则换位
				if (array[j + 1] > array[j]) {
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
					flag = 1;
				}
			}
			if (flag == 0) {
				break;
			}
		}
		for (int i : array) {
			System.out.println(i);
		}*/
	}
}

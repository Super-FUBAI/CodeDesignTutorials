package com.fubai.builderMode.house;

/**
 * @ClassTitle: Worker
 * @ProjectName Gof_23
 * @Description:
 * @Author Sangsang
 * @Date 2020/7/28
 * @Time 18:52
 */
public class Worker extends HouseBuilder {
	private House house;

	public Worker() {
		this.house = new House();
	}

	@Override
	void buildA() {
		house.setBuildA("地基");
		System.out.println("地基");
	}

	@Override
	void buildB() {
		house.setBuildB("钢筋");
		System.out.println("钢筋");

	}

	@Override
	void buildC() {
		house.setBuildC("铺电线");
		System.out.println("铺电线");
	}

	@Override
	void buildD() {
		house.setBuildD("粉刷");
		System.out.println("粉刷");
	}

	@Override
	House getHouse() {
		return house;
	}
}
class Director{
	public House product(Worker worker){
		worker.buildA();
		worker.buildB();
		worker.buildC();
		worker.buildD();

		return worker.getHouse();
//		System.out.println(worker.getHouse().toString());
	}

	public static void main(String[] args) {
		Director p = new Director();
		p.product(new Worker());
	}
}

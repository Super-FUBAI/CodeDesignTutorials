package com.fubai.builderMode.house;

/**
 * @ClassTitle: House
 * @ProjectName Gof_23
 * @Description:
 * @Author Sangsang
 * @Date 2020/7/28
 * @Time 18:49
 */
public class House {
	private String buildA;
	private String buildB;
	private String buildC;
	private String buildD;

	public String getBuildA() {
		return buildA;
	}

	public House setBuildA(String buildA) {
		this.buildA = buildA;
		return this;
	}

	public String getBuildB() {
		return buildB;
	}

	public House setBuildB(String buildB) {
		this.buildB = buildB;
		return this;
	}

	public String getBuildC() {
		return buildC;
	}

	public House setBuildC(String buildC) {
		this.buildC = buildC;
		return this;
	}

	public String getBuildD() {
		return buildD;
	}

	public House setBuildD(String buildD) {
		this.buildD = buildD;
		return this;
	}

	@Override
	public String toString() {
		return "House{" +
				"buildA='" + buildA + '\'' +
				", buildB='" + buildB + '\'' +
				", buildC='" + buildC + '\'' +
				", buildD='" + buildD + '\'' +
				'}';
	}
}

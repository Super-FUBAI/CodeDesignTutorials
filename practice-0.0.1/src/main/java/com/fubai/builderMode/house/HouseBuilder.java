package com.fubai.builderMode.house;

/**
 * @ClassTitle: HouseBuilder
 * @ProjectName Gof_23
 * @Description:
 * @Author Sangsang
 * @Date 2020/7/28
 * @Time 18:46
 */
public abstract class HouseBuilder {
	abstract void buildA();
	abstract void buildB();
	abstract void buildC();
	abstract void buildD();

	abstract House getHouse();
}

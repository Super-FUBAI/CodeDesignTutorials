package com.fubai.builderMode.kfc;

/**
 * @ClassTitle: Meal
 * @ProjectName Gof_23
 * @Description: 肯德基套餐
 * @Author Sangsang
 * @Date 2020/7/28
 * @Time 17:55
 */
public class Meal {
	private String pasta = "意大利面食";

	private String drink = "随机饮料";

	private String dishes = "随机菜肴";

	public String getPasta() {
		return pasta;
	}

	public Meal setPasta(String pasta) {
		this.pasta = pasta;
		return this;
	}

	public String getDrink() {
		return drink;
	}

	public Meal setDrink(String drink) {
		this.drink = drink;
		return this;
	}

	public String getDishes() {
		return dishes;
	}

	public Meal setDishes(String dishes) {
		this.dishes = dishes;
		return this;
	}

	@Override
	public String toString() {
		return "Meal{" +
				"pasta='" + pasta + '\'' +
				", drink='" + drink + '\'' +
				", dishes='" + dishes + '\'' +
				'}';
	}
}

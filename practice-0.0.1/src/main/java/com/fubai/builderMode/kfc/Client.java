package com.fubai.builderMode.kfc;

/**
 * @ClassTitle: Client
 * @ProjectName Gof_23
 * @Description: 客人
 * @Author Sangsang
 * @Date 2020/7/28
 * @Time 18:07
 */
public class Client extends MealBuilder {
	private Meal meal = new Meal();
	@Override
	Meal builderPasta(String p) {

		return null;
	}

	@Override
	Meal builderDrink(String d) {
		return null;
	}

	@Override
	Meal builderDishes(String d) {
		return null;
	}
}

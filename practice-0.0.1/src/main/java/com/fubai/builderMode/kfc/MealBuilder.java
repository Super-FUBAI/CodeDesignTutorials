package com.fubai.builderMode.kfc;

/**
 * @ClassTitle: MealBuilder
 * @ProjectName Gof_23
 * @Description: 建造者
 * @Author Sangsang
 * @Date 2020/7/28
 * @Time 18:04
 */
public abstract class MealBuilder {
	abstract Meal builderPasta(String p);
	abstract Meal builderDrink(String d);
	abstract Meal builderDishes(String d);

}

package cn.bugstack.design.test;

import cn.bugstack.design.NetMall;
import cn.bugstack.design.impl.JDNetMall;
import cn.bugstack.design.impl.TaoBaoNetMall;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 模板模式
 *
 * @author 小傅哥，微信：fustack
 * @description 测试类
 * @date 2022/5/12
 * @github https://github.com/fuzhengwei/CodeDesignTutorials
 * @Copyright 公众号：bugstack虫洞栈 | 博客：https://bugstack.cn - 沉淀、分享、成长，让自己和他人都能有所收获！
 */
public class ApiTest {

    public Logger logger = LoggerFactory.getLogger(ApiTest.class);

    /**
     * 测试链接
     * 京东；https://item.jd.com/100008348542.html
     * 淘宝；https://detail.tmall.com/item.html
     * 当当；http://product.dangdang.com/1509704171.html
     */
    @Test
    public void test_NetMall() {
        NetMall netMall = new JDNetMall("1000001", "*******");
        String base64 = netMall.generateGoodsPoster("https://item.jd.com/100008348542.html");
        logger.info("测试结果：{}", base64);

        /*注意:这里访问需要真实的登录*/
        NetMall netTBMall = new TaoBaoNetMall("1000001", "*******");
        String skuUrlTB = "https://detail.tmall.com/item.htm?spm=a230r.1.14.1.32df4d1filNXjJ&id=683044940649&ns=1&abbucket=15&sku_properties=10004:709990523;5919063:6536025";
        String base64_tb = netTBMall.generateGoodsPoster(skuUrlTB);
        logger.info("测试结果：{}", base64_tb);


    }

}

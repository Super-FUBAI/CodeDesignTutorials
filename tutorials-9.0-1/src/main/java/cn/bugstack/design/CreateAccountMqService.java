package cn.bugstack.design;

import cn.bugstack.design.mq.CreateAccount;
import com.alibaba.fastjson.JSON;

public class CreateAccountMqService {

    public void onMessage(String message) {

        CreateAccount mq = JSON.parseObject(message, CreateAccount.class);

        mq.getNumber();
        mq.getAccountDate();

        // ... 处理自己的业务
    }

}

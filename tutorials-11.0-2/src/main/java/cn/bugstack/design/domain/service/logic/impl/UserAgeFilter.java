package cn.bugstack.design.domain.service.logic.impl;

import cn.bugstack.design.domain.service.logic.BaseLogic;

import java.util.Map;
/**
* 用户年龄过滤器
* @author Hush.
* @date 2022/9/14 11:17
*/
public class UserAgeFilter extends BaseLogic {

    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("age");
    }

}

package cn.bugstack.design.domain.service.logic.impl;

import cn.bugstack.design.domain.service.logic.BaseLogic;

import java.util.Map;

/**
* 用户性别过滤器
* @author Hush.
* @date 2022/9/14 11:17
*/
public class UserGenderFilter extends BaseLogic {

    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("gender");
    }

}
